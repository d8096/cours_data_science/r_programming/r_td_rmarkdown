---
title: "Exemple R Markdown"
output: html_document
date: '2022-10-11'
author: 'Mathilde Fruchart, Antoine Lamer'
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# R Graphics 

### Chargements 

Chargements des données en {cache = TRUE} pour optimiser le temps de chargement.

Chargement des **packages** (sans afficher le code, ni les warnings)

```{r packages, warning=FALSE, include=FALSE}
library(dplyr)
library(ggplot2)
```

------------------------------------------------------------------------

Chargement des **données** (le chemin est celui dans lequel est enregistrer ce fichier).

*Afficher la sortie du code (résultat) uniquement*

```{r data, echo=FALSE, warning=FALSE, cache=TRUE}
data_src = read.csv2("data_220929.csv")
head(data_src)
```

### Data Management

Format des variables SEXE, ASA en factor

*Afficher le code (et l'exécuter)*

```{r DM, echo=TRUE, warning=FALSE, dependson='data'}
data_src$sexe = factor(data_src$sexe)
data_src$asa = factor(data_src$asa)
levels(data_src$asa) = c("ASA1", "ASA2", "ASA3", "ASA4", "ASA5")
```

## Graphics

### Histogramme du poids

*Ne pas afficher le code, seulement le résultat*

```{r histo, echo=FALSE, message=FALSE, warning=FALSE, dependson='data'}
data_src %>%
  filter(poids < 200) %>%
  ggplot(aes(x=poids)) + 
  geom_histogram(aes(y=..density..),color="darkblue", fill="lightblue") 
```

### Courbe de densité du poids en fonction du sexe

*Afficher le code ET le résultat*

```{r poids_sexe, echo=TRUE, message=FALSE, warning=FALSE, dependson='data'}
data_src %>%
  filter(poids < 200) %>%
  ggplot(aes(poids, colour = sexe)) +
  geom_density(adjust = 1)
```
